#
# vtkFiltersMomentInvariants
#

vtk_fetch_module(MomentInvariants
  "MomentInvariants filter"
  GIT_REPOSITORY https://gitlab.kitware.com/vtk/MomentInvariants.git
  GIT_TAG a3cdbae4eb453193ce5810f13119cf234b28b236
  )
